<?php require_once("includes/conn.php");?>
<?php require_once("includes/functions.php");?>
<?php
   $errror = "";
    if(isset($_POST["submit"])){

        $plateno = $_POST["plateNo"];
        $current_color = $_POST["current_color"];
        $target_color = $_POST["target_color"];

        $required_fields = ["plateNo", "current_color", "target_color"];
        validate_presences($required_fields);

        if(empty($error)){
            $query = "INSERT INTO `paintjobs`(`plateNo`, `current_color`, `target_color`) VALUES ('$plateno', '$target_color', '$current_color')";
            $result = mysqli_query($connection, $query);

            if($result){
                header("Location: paintJobs.php");
            }else {
                $error = "Error";
            }
        }else{

        }
    }
?>
<?php include("includes/header.php");?>

    <div class="ban">
        <div class="topnav">
            <B>
            <a class="active" href="newPaintJob.php">NEW PAINT JOB</a></li>
            <a href="paintJobs.php">PAINT JOBS</a>
            </B>
        </div>
    </div>
    
    <h1 class="head1">New Paint Job</h1>
    <div class="carPic">
        <?php ?>
        <img src="images/default.png" alt="Image" style="height: 20%; width: 30%;">
        <img src="images/arrow.png" alt="Image" style="margin-bottom: 50px;">
        <img src="images/default.png" alt="Image" style="height: 20%; width: 30%;">
    </div>
    <div class="carDetails">
        <div>
            <?php
                if(empty($error)){

                }else{
                    echo "Fields cant be Blank";
                }
            ?>
        </div>
        <h4>Car Details</h4>
        <form action="newPaintJob.php" method="post">
            <table>
                <tr>
                    <td>Plate No.</td>
                    <td><input type="text" name="plateNo" required="required" value="" autofocus/></td>
                </tr>
                <tr>
                    <td>Current Color</td>
                    <td>
                        <select name="current_color">
                            <option value=""></option>
                            <option value="Red" >Red</option>
                            <option value="Blue" >Blue</option>
                            <option value="Green" >Green</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Target Color</td>
                    <td>
                        <select name="target_color">
                            <option value=""></option>
                            <option value="Red">Red</option>
                            <option value="Blue">Blue</option>
                            <option value="Green">Green</option>
                        </select>
                    </td>
                </tr>
            </table>
            <input class="submit" type="submit" name="submit" value="Submit" />
        <form>
    </div>

<?php include("includes/footer.php");?>
