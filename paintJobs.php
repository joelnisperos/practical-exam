<?php require_once("includes/conn.php");?>
<?php include("includes/header.php");?>

    <div class="ban">
        <div class="topnav">
            <B>
            <a href="newPaintJob.php">NEW PAINT JOB</a></li>
            <a  class="active" href="paintJobs.php">PAINT JOBS</a>
            </B>
        </div>
    </div>
    <h1 class="head1">Paint Jobs</h1>
    <div class="carDetails">
        <h4 style="margin-left: -280px;">Paint Jobs in Progress</h4>
        <table class="progress">
            <tr>
                <th>Plate No.</th>
                <th>Current Color</th>
                <th>Target Color</th>
                <th>Action</th>
            </tr>
                <?php 
                    $query = "SELECT * FROM `paintjobs` ORDER BY `id` ASC LIMIT 5";
                    $result = mysqli_query($connection, $query);
                    while($cars = mysqli_fetch_assoc($result)){?>
                    <tr>
                        <td><?php echo $cars["plateNo"];?></td>
                        <td><?php echo $cars["current_color"];?></td>
                        <td><?php echo $cars["target_color"];?></td>
                        <td>
                            <!-- <a href="#" class="mark" onclick="document.getElementById('myform').submit()">Mark as Completed</a> -->
                            <a href="#" class="mark" >Mark as Completed</a>
                        </td>
                    </tr><?php
                 } ?>
        </table>
        <br/><br/><br/><br/><br/><br/><br/><br/>
            <h4 style="margin-left: -360px;">Paint Queue</h4>
        <table class="progress">
            <tr>
                <th>Plate No.</th>
                <th>Current Color</th>
                <th>Target Color</th>
                <th></th>
            </tr>
                <?php 
                    $query = "SELECT * FROM paintjobs ORDER BY `id` DESC LIMIT 3";
                    $result = mysqli_query($connection, $query);
                    while($cars = mysqli_fetch_assoc($result)){?>
                    <tr>
                        <td><?php echo $cars["plateNo"];?></td>
                        <td><?php echo $cars["current_color"];?></td>
                        <td><?php echo $cars["target_color"];?></td>
                        <td></td>
                    </tr><?php
                 } ?>
        </table>
    </div>


<?php include("includes/footer.php");?>
