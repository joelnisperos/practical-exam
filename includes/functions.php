<?php

    function validate_presences($required_fields){
        global $error;
        foreach ($required_fields as $field) {
            $value = trim($_POST[$field]);
            if (!has_presence($value)){
                $error =  $field . " cant be blank";
            }
        }
    }

    function has_presence($value){
        return isset($value) && $value !== "";
    }
?>